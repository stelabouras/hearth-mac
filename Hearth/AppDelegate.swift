//
//  AppDelegate.swift
//  Hearth
//
//  Created by Stelios Petrakis on 30/12/2017.
//  Copyright © 2017 Horizon Video Technologies. All rights reserved.
//

import Cocoa
import ServiceManagement
import Sparkle

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate, NSUserNotificationCenterDelegate {

    static public let hearthOpenTutorialNotification = "hearthOpenTutorialNotification"
    static public let hearthEternumAPIKeyUpdatedNotification = "hearthEternumAPIKeyUpdatedNotification"

    private var tutorialWindowController : NSWindowController? = nil
    private var preferencesWindowController : NSWindowController? = nil
    
    private var isLoading = false
    private var isOffline = false
    
    var initialScanFinished = false
    
    let delayBeforeDismissingNotification : TimeInterval = 5
    
    let hearthTutorialKey = "hearthTutorialKey"

    let statusItem = NSStatusBar.system.statusItem(withLength:NSStatusItem.squareLength)
    
    let reachability = Reachability()!
    
    let updater = SUUpdater.shared()!
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {

        guard HearthManager.shared.createHearthDirectoryIfNeeded() else {
            
            showNotification(title: "Error while trying to create Hearth folder",
                             informativeText: "Please re-install the app",
                             autoDismiss: false)
            
            return
        }
        
        updateFinderExtensionBasedOnPreference()
        
        // \m/
        spawnDaemons()
        
        updateMenuIcon()
        
        constructMenu()
        
        updateStartupItemBasedOnPreference()
        
        // Listen for changes happening by the finder extension
        HearthManager.shared.sharedDefaults?.addObserver(self, forKeyPath: HearthManager.shared.hearthNotificationErrorKey, options: .new, context: nil)
        HearthManager.shared.sharedDefaults?.addObserver(self, forKeyPath: HearthManager.shared.hearthNotificationPathKey, options: .new, context: nil)
        HearthManager.shared.sharedDefaults?.addObserver(self, forKeyPath: HearthManager.shared.hearthNotificationLinkKey, options: .new, context: nil)

        // Listen for changes made in preferences
        HearthManager.shared.sharedDefaults?.addObserver(self, forKeyPath: HearthManager.shared.hearthStartupIsDisabledKey, options: .new, context: nil)
        HearthManager.shared.sharedDefaults?.addObserver(self, forKeyPath: HearthManager.shared.hearthFinderIsDisabledKey, options: .new, context: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(openTutorialNotification(notification:)),
                                               name: NSNotification.Name(rawValue: AppDelegate.hearthOpenTutorialNotification),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(apiKeyUpdatedNotification(notification:)),
                                               name: NSNotification.Name(rawValue: AppDelegate.hearthEternumAPIKeyUpdatedNotification),
                                               object: nil)

        // Show tutorial window on first launch
        if UserDefaults.standard.bool(forKey: hearthTutorialKey) == false {
            
            openTutorial()
            
            UserDefaults.standard.set(true, forKey: hearthTutorialKey)
        }
        
        reachability.whenReachable = { reachability in
        
            self.isOffline = false
            
            self.updateMenuIcon()
        }
        reachability.whenUnreachable = { _ in
            
            self.isOffline = true

            self.updateMenuIcon()
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    func applicationWillTerminate(_ notification: Notification) {

        // Signal that the Mac app is not running (used by the Finder extension)
        HearthManager.shared.sharedDefaults?.set(false, forKey: HearthManager.shared.hearthIsIPFSReadyKey)
        
        // Disable the finder extension
        disableFinderExtension()
        
        // In case user logs out
        killDaemons()
    }
    
    // MARK: - KVO
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {

        if keyPath == HearthManager.shared.hearthNotificationPathKey || keyPath == HearthManager.shared.hearthNotificationLinkKey {
            
            guard let path = HearthManager.shared.sharedDefaults?.string(forKey: HearthManager.shared.hearthNotificationPathKey),
                  let link = HearthManager.shared.sharedDefaults?.string(forKey: HearthManager.shared.hearthNotificationLinkKey) else {
                
                return
            }
            
            self.showNotification(title: "Copied link on '" + path + "'.",
                                  informativeText: "A link to '" + path + "' has been copied to your clipboard.",
                                  linkPayload: link)

            HearthManager.shared.sharedDefaults?.removeObject(forKey: HearthManager.shared.hearthNotificationPathKey)
            HearthManager.shared.sharedDefaults?.removeObject(forKey: HearthManager.shared.hearthNotificationLinkKey)
        }
        else if keyPath == HearthManager.shared.hearthNotificationErrorKey {
            
            guard let errorLocalizedDescription = HearthManager.shared.sharedDefaults?.string(forKey: HearthManager.shared.hearthNotificationErrorKey) else {
                
                return
            }
        
            self.showNotification(title: "Error generating link",
                                  informativeText: errorLocalizedDescription)
            
            HearthManager.shared.sharedDefaults?.removeObject(forKey: HearthManager.shared.hearthNotificationErrorKey)
        }
        else if keyPath == HearthManager.shared.hearthStartupIsDisabledKey {
            
            updateStartupItemBasedOnPreference()
        }
        else if keyPath == HearthManager.shared.hearthFinderIsDisabledKey {

            updateFinderExtensionBasedOnPreference()
        }
    }
    
    // MARK: - Actions

    @objc func apiKeyUpdatedNotification(notification: NSNotification?) {
        
        spawnDaemons()
    }

    @objc func openTutorialNotification(notification: NSNotification?) {
        
        openTutorial()
    }
    
    @objc func clickedAbout(_ sender: Any?) {

        visitURL(urlString: "https://hearth.eternum.io")
    }

    @objc func clickedCheckUpdates(_ sender: Any?) {
        
        updater.checkForUpdates(sender)
    }
    
    @objc func clickedPreferences(_ sender: Any?) {

        if let preferencesWindowController = preferencesWindowController {

            preferencesWindowController.showWindow(self)
            NSApp.activate(ignoringOtherApps: true)
            
            return
        }
        
        let storyboard = NSStoryboard(name: NSStoryboard.Name(rawValue: "Main"), bundle: nil)
        
        guard let windowController = storyboard.instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "PreferencesWindow")) as? NSWindowController else {
            
            return
        }
        
        preferencesWindowController = windowController
        
        windowController.showWindow(self)
        
        NSApp.activate(ignoringOtherApps: true)
    }
    
    @objc func clickedSettings(_ sender: Any?) {

        visitURL(urlString: "https://eternum.io")
    }
    
    @objc func clickedQuit(_ sender: Any?) {

        terminateDaemonsAndHearth()
    }

    @objc func copyHearthLink(_ sender: Any?) {
        
        print("copyHearthLink")

        guard initialScanFinished else {
            return
        }
        
        HRTApiHandler.shared.generateHearthLink(relativePath: "", completionClosure: { (link, error) in
            
            print("copyHearthLink -> completion")

            guard let link = link else {
                
                print("Error while generating link")
                
                if let error = error {
                    
                    self.showNotification(title: "Error generating link",
                                          informativeText: error.localizedDescription)

                    print("Error: \(error)")
                }
                else {
                    
                    self.showNotification(title: "Error generating link",
                                          informativeText: "Please try again.")
                }
                
                return
            }
            
            let pasteBoard = NSPasteboard.general
            pasteBoard.clearContents()
            pasteBoard.writeObjects([link as NSString])

            self.showNotification(title: "Copied link on the Hearth folder.",
                                  informativeText: "A link to the Hearth folder has been copied to your clipboard.",
                                  linkPayload: link)
        })
    }
    
    @objc func clickedOpen(_ sender: Any?) {
    
        guard let hearthDirectoryURL = HearthManager.shared.hearthDirectoryURL else {
            return
        }
        
        NSWorkspace.shared.open(hearthDirectoryURL)
    }
    
    // MARK: - Private
    
    func showDialog(text: String, description: String) -> Bool {
        
        let alert = NSAlert()
        alert.messageText = text
        alert.informativeText = description
        alert.alertStyle = .warning
        alert.addButton(withTitle: "OK")
        return alert.runModal() == .alertFirstButtonReturn
    }
    
    func openTutorial() {
        
        guard !isOffline else {
            
            _ = showDialog(text: "Offline",
                           description: "Please connect to the Internet before accessing the tutorial.")
            
            return
        }
        
        if let tutorialWindowController = tutorialWindowController {
            
            tutorialWindowController.showWindow(self)
            NSApp.activate(ignoringOtherApps: true)
            
            return
        }
        
        let storyboard = NSStoryboard(name: NSStoryboard.Name(rawValue: "Main"), bundle: nil)
        
        guard let windowController = storyboard.instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "TutorialWindow")) as? NSWindowController else {
            
            return
        }
        
        tutorialWindowController = windowController
        
        windowController.window?.setContentSize(NSSize(width: 600, height: 600))
        windowController.showWindow(self)
        
        NSApp.activate(ignoringOtherApps: true)
    }
    
    func terminateDaemonsAndHearth() {
        
        HRTApiHandler.shared.exitHearth { (error) in
            
            if error != nil {
                print("error=\(error!)")
            }
            
            NSApplication.shared.terminate(self)
        }
    }
    
    func showNotification(title : String, informativeText : String, linkPayload : String? = nil, autoDismiss : Bool = true) {
        
        DispatchQueue.main.async {
            
            let notification = NSUserNotification()
            notification.title = title
            notification.informativeText = informativeText
            
            if let linkPayload = linkPayload {

                notification.userInfo = [ "link" : linkPayload ]
                notification.hasActionButton = true
                notification.actionButtonTitle = "Open"
                
                NSUserNotificationCenter.default.delegate = self
            }
            else {
                
                notification.hasActionButton = false
            }
            
            print("showNotification about to deliver notification: \(title)")
            
            NSUserNotificationCenter.default.deliver(notification)
            
            if autoDismiss {
                
                DispatchQueue.main.asyncAfter(deadline: .now() + self.delayBeforeDismissingNotification, execute: {

                    NSUserNotificationCenter.default.removeDeliveredNotification(notification)
                })
            }
        }
    }
    
    func spawnDaemons() {
        
        // Kill any previous opened deamons
        killDaemons()
        
        // Add the current resourcePath (the directory where the daemons live)
        // to the current PATH env variable so that hearth deamon will be able
        // to find and launch the IPFS deamon
        let resourcePath = Bundle.main.resourcePath!
        
        var path = ""
        
        if let existingPATH = ProcessInfo.processInfo.environment["PATH"] {
            
            path = existingPATH + ":" + resourcePath
        }
        else {
            
            path = resourcePath
        }
        
        // Create the Process (ex-NSTask) that launches
        // the hearth deamon (Rust app) with full backtrace
        // for debugging reasons
        let task = Process()
        task.environment = [ "PATH" : path, "RUST_BACKTRACE" : "full" ]
        //task.arguments = ["-d"]

        if let apiKey = HearthManager.shared.sharedDefaults?.string(forKey: HearthManager.shared.eternumAPIKey) {
            task.arguments = ["-k" + apiKey]
        }
        
        task.launchPath = resourcePath + "/hearth"
        
        // Pipe the process output so that we can read it
        // and respond to events from the deamon (just as syncing)
        let pipe = Pipe()
        task.standardOutput = pipe
        let outHandle = pipe.fileHandleForReading
        outHandle.waitForDataInBackgroundAndNotify()
        
        var obs1 : NSObjectProtocol!
        obs1 = NotificationCenter.default.addObserver(forName: NSNotification.Name.NSFileHandleDataAvailable,
                                                                       object: outHandle, queue: nil) {  notification -> Void in
                                                                        
                                                                        let data = outHandle.availableData
                                                                        
                                                                        if data.count > 0 {
                                                                            
                                                                            if let str = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                                                                                
                                                                                // Split the output into separate lines
                                                                                // and read them so that we can detect
                                                                                // various Hearth messages
                                                                                str.enumerateLines { line, _ in
                                                                                    
                                                                                    print("\(line)")
                                                                                    
                                                                                    self.detectMessage(hearthMessage: line)
                                                                                }
                                                                            }
                                                                            
                                                                            outHandle.waitForDataInBackgroundAndNotify()
                                                                            
                                                                        } else {
                                                                            
                                                                            print("EOF on stdout from process")
                                                                            NotificationCenter.default.removeObserver(obs1)
                                                                        }
        }
        
        var obs2 : NSObjectProtocol!
        obs2 = NotificationCenter.default.addObserver(forName: Process.didTerminateNotification,
                                                                       object: task, queue: nil) { notification -> Void in
                                                                        print("terminated")
                                                                        NotificationCenter.default.removeObserver(obs2)
        }
        
        task.launch()
    }
    
    func updateFinderExtensionBasedOnPreference() {
        
        if let hearthFinderDisabledValue = HearthManager.shared.sharedDefaults?.bool(forKey: HearthManager.shared.hearthFinderIsDisabledKey) {
            
            if hearthFinderDisabledValue == true {
                
                disableFinderExtension()
            }
            else {
                
                enableFinderExtension()
            }
        }
    }
    
    func enableFinderExtension() {

        // Ref: https://stackoverflow.com/questions/31176942/how-to-enable-findersync-extension-in-the-system-preference-in-cocoa-objective
        
        let task = Process()
        task.launchPath = "/usr/bin/pluginkit"
        task.arguments = ["-euse", "-icom.hvt.Hearth.finder"]
        task.launch()
        task.waitUntilExit()
    }

    func disableFinderExtension() {
        
        let task = Process()
        task.launchPath = "/usr/bin/pluginkit"
        task.arguments = ["-eignore", "-icom.hvt.Hearth.finder"]
        task.launch()
        task.waitUntilExit()
    }
    
    func killDaemons() {
        
        let task = Process()
        task.launchPath = "/usr/bin/killall"
        task.arguments = ["hearth", "ipfs"]
        task.launch()
        task.waitUntilExit()
    }
    
    func detectMessage(hearthMessage: String) {
        
        if hearthMessage.contains("Change detected in filesystem") ||
            hearthMessage.contains("Adding files to IPFS") {
            
            isLoading = true

            updateMenuIcon()
        }
        else if hearthMessage.contains("Finished adding files to IPFS") {
            
            isLoading = false

            HearthManager.shared.sharedDefaults?.set(true,
                                                     forKey: HearthManager.shared.hearthIsIPFSReadyKey)

            updateMenuIcon()
            
            if !initialScanFinished {
                
                initialScanFinished = true
                
                constructMenu()
            }
        }
    }

    func visitURL(urlString: String) {

        guard let url = URL(string: urlString) else {
            
            return
        }
        
        NSWorkspace.shared.open(url)
    }
    
    func constructMenu() {
        
        let menu = NSMenu()
        
        menu.addItem(NSMenuItem(title: "Open Hearth folder", action: #selector(AppDelegate.clickedOpen(_:)), keyEquivalent: ""))
        
        if initialScanFinished {
            
            menu.addItem(NSMenuItem.separator())
            menu.addItem(NSMenuItem(title: "Copy Hearth folder link", action: #selector(AppDelegate.copyHearthLink(_:)), keyEquivalent: ""))
        }
        
        menu.addItem(NSMenuItem.separator())
        menu.addItem(NSMenuItem(title: "Preferences...", action: #selector(AppDelegate.clickedPreferences(_:)), keyEquivalent: ","))

        menu.addItem(NSMenuItem.separator())
        menu.addItem(NSMenuItem(title: "About Hearth", action: #selector(AppDelegate.clickedAbout(_:)), keyEquivalent: ""))
        menu.addItem(NSMenuItem.separator())
        menu.addItem(NSMenuItem(title: "Check for Updates...", action: #selector(AppDelegate.clickedCheckUpdates(_:)), keyEquivalent: ""))
        menu.addItem(NSMenuItem.separator())
        menu.addItem(NSMenuItem(title: "Quit Hearth", action: #selector(AppDelegate.clickedQuit(_:)), keyEquivalent: ""))
        
        statusItem.menu = menu
    }
    
    // MARK: - Menu Icon
    
    func updateMenuIcon() {
        
        DispatchQueue.main.async {
            
            if let button = self.statusItem.button {
                
                var menuName = "menuicon"
                
                if self.isLoading {
                    
                    menuName += "-loading"
                }
                
                if self.isOffline {
                    
                    menuName += "-disabled"
                }
                
                button.image = NSImage(named:NSImage.Name(menuName))
            }
        }
    }
    
    // MARK: - Login item
    
    // References:
    //
    // https://www.delitestudio.com/start-dockless-apps-at-login-with-app-sandbox-enabled/
    // http://rhult.github.io/articles/sandboxed-launch-on-login/
    // https://stackoverflow.com/questions/44030354/macos-swift-how-to-properly-add-application-as-login-item
    // https://blog.timschroeder.net/2012/07/03/the-launch-at-login-sandbox-project/
    // https://theswiftdev.com/2017/10/27/how-to-launch-a-macos-app-at-login/
    // https://christiantietze.de/posts/2015/01/xpc-helper-sandboxing-mac/
    // https://stackoverflow.com/questions/16707136/cant-code-sign-helper-app-properly
    
    let kLoginHelperBundleIdentifier = "com.hvt.Hearth.helper"
    
    func enableStartupItem() {
        
        let url = Bundle.main.bundleURL.appendingPathComponent("Contents/Library/LoginItems/helper.app")
        
        if LSRegisterURL(url as CFURL, true) != noErr {
            
            print("LSRegisterURL failed!")
        }
        
        if !SMLoginItemSetEnabled(kLoginHelperBundleIdentifier as CFString, true) {

            print("SMLoginItemSetEnabled(..., true) failed")
        }
        else {

            print("SMLoginItemSetEnabled(..., true) succeeded")
        }
    }
    
    func disableStartupItem() {

        if !SMLoginItemSetEnabled(kLoginHelperBundleIdentifier as CFString, false) {
            
            print("SMLoginItemSetEnabled(..., false) failed")
        }
        else {
            
            print("SMLoginItemSetEnabled(..., false) succeeded")
        }
    }
    
    func updateStartupItemBasedOnPreference() {
        
        if let hearthStartupDisabledValue = HearthManager.shared.sharedDefaults?.bool(forKey: HearthManager.shared.hearthStartupIsDisabledKey),
            hearthStartupDisabledValue == true {
                
                disableStartupItem()
        }
        else {
            
            enableStartupItem()           
        }
    }
    
    // MARK: - NSUserNotificationCenterDelegate
    
    func userNotificationCenter(_ center: NSUserNotificationCenter, didActivate notification: NSUserNotification) {
        
        guard notification.activationType == .actionButtonClicked,
            let linkPayload = notification.userInfo?["link"] as? String else {
            return
        }
        
        visitURL(urlString: linkPayload)
    }
}

