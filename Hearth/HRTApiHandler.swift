//
//  HRTApiHandler.swift
//  Hearth
//
//  Created by Stelios Petrakis on 26/01/2018.
//  Copyright © 2018 Horizon Video Technologies. All rights reserved.
//

import Foundation

class HRTApiHandler {
 
    static let shared = HRTApiHandler()

    let ipfsService = "https://www.eternum.io"
    let deamonIP    = "localhost"
    let deamonPort  = 26484
    
    fileprivate func buildAPIEndpoint() -> String {
        
        return "http://" + deamonIP + ":" + String(deamonPort) + "/api/"
    }
    
    func exitHearth(completionClosure: @escaping (Error?) -> ()) {
        
        let url = URL(string: buildAPIEndpoint() + "exit/")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            completionClosure(error)
            
            }.resume()
    }

    func preloadLink(link : String) {

        DispatchQueue.global(qos: .background).async {

            let url = URL(string: link)!
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                //print("\(response) \(error)")
                
                }.resume()
        }
    }
    
    func generateHearthLink(relativePath : String, completionClosure: @escaping (String?, Error?) -> ()) {
        
        print("generateHearthLink")
        
        let urlString = buildAPIEndpoint() + "path/"
        
        var components = URLComponents(string: urlString)!
        
        if relativePath.count > 0 {

            components.queryItems = [
                URLQueryItem(name: "path", value: relativePath)
            ]
        }
        
        var request = URLRequest(url: components.url!)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            print("generateHearthLink -> dataTask completion")
            
            guard error == nil else {
                
                completionClosure(nil, error)
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse,
                httpResponse.statusCode == 200 else {
                
                completionClosure(nil, nil)
                return
            }
            
            guard let data = data,
                let json = try? JSONSerialization.jsonObject(with: data,
                                                             options: []) as? [String: Any],
                let hash = json!["hash"] as? String else {
                
                completionClosure(nil, nil)
                return
            }

            let link = self.ipfsService + "/ipfs/" + hash
            
            completionClosure(link, nil)
            
            self.preloadLink(link: link)
            
            }.resume()

    }
}
