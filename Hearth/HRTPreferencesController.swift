//
//  HRTPreferencesController.swift
//  Hearth
//
//  Created by Stelios Petrakis on 20/02/2018.
//  Copyright © 2018 Horizon Video Technologies. All rights reserved.
//

import Cocoa

class HRTPreferencesController: NSViewController {

    @IBOutlet var finderButton: NSButton!
    @IBOutlet var startupButton: NSButton!
    @IBOutlet var versionLabel: NSTextField!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        if  let infoDictionary = Bundle.main.infoDictionary,
            let version = infoDictionary["CFBundleShortVersionString"] as? String {
            
            versionLabel.stringValue = "v" + version
        }
        
        if let hearthStartupDisabledValue = HearthManager.shared.sharedDefaults?.bool(forKey: HearthManager.shared.hearthStartupIsDisabledKey) {
            
            startupButton.state = (hearthStartupDisabledValue == true ? .off : .on)
        }

        if let hearthFinderDisabledValue = HearthManager.shared.sharedDefaults?.bool(forKey: HearthManager.shared.hearthFinderIsDisabledKey) {
            
            startupButton.state = (hearthFinderDisabledValue == true ? .off : .on)
        }
    }
    
    @IBAction func startupButtonClicked(_ sender: Any) {
        
        HearthManager.shared.sharedDefaults?.set(startupButton.state == .off, forKey: HearthManager.shared.hearthStartupIsDisabledKey)
    }
    
    @IBAction func finderButtonClicked(_ sender: Any) {
        
        HearthManager.shared.sharedDefaults?.set(finderButton.state == .off, forKey: HearthManager.shared.hearthFinderIsDisabledKey)
    }
    
    @IBAction func feedbackButtonClicked(_ sender: Any) {
        
        guard let url = URL(string: "mailto:hearth@eternum.io?subject=Hearth") else {
            
            return
        }
        
        NSWorkspace.shared.open(url)
    }
    
    @IBAction func tutorialButtonClicked(_ sender: Any) {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppDelegate.hearthOpenTutorialNotification), object: nil)
    }
}
