//
//  HRTTutorialController.swift
//  Hearth
//
//  Created by Stelios Petrakis on 15/02/2018.
//  Copyright © 2018 Horizon Video Technologies. All rights reserved.
//

import Cocoa
import WebKit

class HRTTutorialController: NSViewController, WKNavigationDelegate {

    @IBOutlet var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.wantsLayer = true
        
        if let layer = view.layer {
            
            layer.backgroundColor = NSColor(red: 64.0/256.0, green: 15.0/256.0, blue: 4.0/256.0, alpha: 1.0).cgColor
        }
        
        if NSAppKitVersion.current.rawValue > 1500 {
            webView.setValue(false, forKey: "drawsBackground")
        }
        else {
            webView.setValue(true, forKey: "drawsTransparentBackground")
        }
        
        let hearthLink = "https://hearth.eternum.io/tutorial/mac/"
        let url = URL(string: hearthLink)
        let request = URLRequest(url: url!)
        
        webView.navigationDelegate = self
        webView.load(request)
    }
    
    // MARK: - WKNavigationDelegate
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Swift.Void)
    {
        if(navigationAction.navigationType == .linkActivated)
        {
            if let requestURL = navigationAction.request.url {
                
                NSWorkspace.shared.open(requestURL)
            }
            
            decisionHandler(.cancel)
            return
        }
        decisionHandler(.allow)
    }
}
