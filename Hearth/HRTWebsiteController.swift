//
//  HRTWebsiteController.swift
//  Hearth
//
//  Created by Stelios Petrakis on 05/03/2018.
//  Copyright © 2018 Horizon Video Technologies. All rights reserved.
//

import Cocoa

class HRTWebsiteController: NSViewController {

    @IBOutlet var learnMoreTextView: NSTextView!
    @IBOutlet var summaryTextView: NSTextView!
    @IBOutlet var apiKeyTextField: NSTextField!
    
    @IBAction func updateButtonClicked(_ sender: Any) {
        
        let updatedAPIKey = apiKeyTextField.stringValue
        
        guard updatedAPIKey.count > 0 else {
            return
        }
        
        if let apiKey = HearthManager.shared.sharedDefaults?.string(forKey: HearthManager.shared.eternumAPIKey),
            apiKey == updatedAPIKey {
            
            return
        }
        
        HearthManager.shared.sharedDefaults?.set(updatedAPIKey,
                                                 forKey: HearthManager.shared.eternumAPIKey)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppDelegate.hearthEternumAPIKeyUpdatedNotification),
                                        object: nil)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        if let apiKey = HearthManager.shared.sharedDefaults?.string(forKey: HearthManager.shared.eternumAPIKey) {
            
            apiKeyTextField.stringValue = apiKey
        }

        let summaryString = summaryTextView.string as NSString
        
        let preferencesRange = summaryString.range(of: "Eternum account preferences")
        
        summaryTextView.textStorage?.addAttribute(.link,
                                                  value: "https://www.eternum.io/account/",
                                                  range: preferencesRange)

        
        let createAccountRange = summaryString.range(of: "you can create one for free")
        
        summaryTextView.textStorage?.addAttribute(.link,
                                                  value: "https://www.eternum.io",
                                                  range: createAccountRange)

        let apiKeyRange = summaryString.range(of: "Eternum API key")
        
        summaryTextView.textStorage?.addAttribute(.link,
                                                 value: "https://www.eternum.io/account/#api-key",
                                                 range: apiKeyRange)

        let learnMoreString = learnMoreTextView.string as NSString
        
        let personalSiteRange = learnMoreString.range(of: "Personal site")

        learnMoreTextView.textStorage?.addAttribute(.link,
                                                  value: "https://hearth.eternum.io/tutorial/mac/#personal-site",
                                                  range: personalSiteRange)
    }
}
