//
//  HearthManager.swift
//  Hearth
//
//  Created by Stelios Petrakis on 30/12/2017.
//  Copyright © 2017 Horizon Video Technologies. All rights reserved.
//

import Cocoa

extension String {
    public func remove(prefix: String) -> String {
        if hasPrefix(prefix) {
            return String(self.dropFirst(prefix.count))
        }
        return self
    }
}

class HearthManager {

    static let shared = HearthManager()

    private let sharedDefaultsSuiteName = "group.com.hvt.hearth"

    public let hearthNotificationErrorKey   = "hearthNotificationErrorKey"
    public let hearthNotificationPathKey    = "hearthNotificationPathKey"
    public let hearthNotificationLinkKey    = "hearthNotificationLinkKey"
    
    public let hearthIsIPFSReadyKey        = "hearthIsIPFSReadyKey"
    
    public let hearthStartupIsDisabledKey  = "hearthStartupIsDisabledKey"
    public let hearthFinderIsDisabledKey   = "hearthFinderIsDisabledKey"
    
    public let eternumAPIKey         = "eternumAPIKey"
    
    private let hearthDirectoryPathKey  = "hearthDirectoryPathKey"
    
    private let defaultHearthFolder     = "Hearth"
    
    public var sharedDefaults : UserDefaults?
    
    var hearthDirectoryURL : URL?

    func createHearthDirectoryIfNeeded() -> Bool {
    
        sharedDefaults = UserDefaults.init(suiteName: sharedDefaultsSuiteName)
        
        var directoryPath = sharedDefaults?.string(forKey: hearthDirectoryPathKey)
        
        if directoryPath == nil {

            // NOTE: We do not use the shared directory between the container app and the extension
            // as for some reason the Finder Sync extension cannot track it.
            
            // So we make the container app non-sandboxed (it won't be distributed over the Mac App Store)
            // and we create a folder in the `/Users/[username]/Hearth`
            //directoryPath = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: "group.com.hvt.hearth")!.path

            directoryPath = NSString(string: "~/" + defaultHearthFolder).expandingTildeInPath
            
            sharedDefaults?.set(directoryPath, forKey: hearthDirectoryPathKey)
        }
        
        hearthDirectoryURL = URL(fileURLWithPath: directoryPath!)
        
        do {
            
            try FileManager.default.createDirectory(atPath: hearthDirectoryURL!.path, withIntermediateDirectories: true, attributes: nil)
            
        } catch let error as NSError {
            
            print("Error creating directory: \(error.localizedDescription)")
            return false
        }
        
        // TODO: Assign an icon to the folder
        // let iconImage = NSImage(named:NSImage.Name("menuicon"))
        // NSWorkspace.shared.setIcon(iconImage, forFile: self.hearthDirectoryURL!.path)
        
        return true
    }
    
    func relativePathForHearthPath(_ path : String) -> String {
        
        guard let hearthDirectoryPath = hearthDirectoryURL?.path else {
                
            return path
        }
        
        return path.remove(prefix: hearthDirectoryPath + "/")
    }
}
