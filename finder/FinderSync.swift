//
//  FinderSync.swift
//  finder
//
//  Created by Stelios Petrakis on 30/12/2017.
//  Copyright © 2017 Horizon Video Technologies. All rights reserved.
//

import Cocoa
import FinderSync

// Ref: https://stackoverflow.com/questions/29754914/osx-finder-sync-extension

class FinderSync: FIFinderSync {

    override init() {
        
        super.init()

        if HearthManager.shared.createHearthDirectoryIfNeeded() {

            // Set up the directory we are syncing.
            FIFinderSyncController.default().directoryURLs = [ HearthManager.shared.hearthDirectoryURL! ]
        }
    }
    
    // MARK: - Primary Finder Sync protocol methods
    
    override func beginObservingDirectory(at url: URL) {
        
        // The user is now seeing the container's contents.
        // If they see it in more than one view at a time, we're only told once.
    }
    
    override func endObservingDirectory(at url: URL) {

        // The user is no longer seeing the container's contents.
    }
    
    // MARK: - Menu and toolbar item support
    
    override var toolbarItemName: String {
        return "Hearth"
    }
    
    override var toolbarItemToolTip: String {
        return "Hearth: Use Hearth specific features"
    }
    
    override var toolbarItemImage: NSImage {
        return NSImage(named:NSImage.Name("finder-icon"))!
    }
    
    override func menu(for menuKind: FIMenuKind) -> NSMenu {

        let menu = NSMenu(title: "")

        guard   let targetURL = FIFinderSyncController.default().targetedURL(),
                let directoryURL = FIFinderSyncController.default().directoryURLs.first else {
                
            if menuKind == .toolbarItemMenu {
                
                menu.addItem(withTitle: "Open Hearth Folder", action: #selector(openHearthFolder(_:)), keyEquivalent: "")
            }
            
            return menu
        }
        
        if targetURL.path != directoryURL.path && !targetURL.path.hasPrefix(directoryURL.path) {

            if menuKind == .toolbarItemMenu {
                
                menu.addItem(withTitle: "Open Hearth Folder", action: #selector(openHearthFolder(_:)), keyEquivalent: "")
            }
            
            return menu
        }
        
        guard HearthManager.shared.sharedDefaults?.bool(forKey: HearthManager.shared.hearthIsIPFSReadyKey) == true,
            let items = FIFinderSyncController.default().selectedItemURLs(), items.count == 1 else {
            
            if menuKind == .toolbarItemMenu {

                let menuItem = NSMenuItem(title: "No options available", action: nil, keyEquivalent: "")
                menuItem.isEnabled = false
            
                menu.addItem(menuItem)
            }
            
            return menu
        }

        if items[0].path == targetURL.path {
            
            return menu
        }
        
        // Produce a menu for the extension.
        let menuItem = NSMenuItem(title: "Copy Hearth link", action: #selector(copyHearthLink(_:)), keyEquivalent: "")
        
        if menuKind != .toolbarItemMenu {
            menuItem.image = NSImage(named:NSImage.Name("finder-icon"))!
        }
        
        menu.addItem(menuItem)
        
        return menu
    }
    
    // MARK: - Private
    
    func visitURL(urlString: String) {
        
        let url = URL(string: urlString)!
        
        NSWorkspace.shared.open(url)
    }
    
    // MARK: - Actions
    
    @IBAction func copyHearthLink(_ sender: AnyObject?) {

        print("copyHearthLink -> completion")

        let items = FIFinderSyncController.default().selectedItemURLs()
        
        guard let item = items?.first else {
            
            return
        }
        
        let path = HearthManager.shared.relativePathForHearthPath(item.path)
        
        HRTApiHandler.shared.generateHearthLink(relativePath: path, completionClosure: { (link, error) in
            
            print("copyHearthLink -> completion")
            
            guard let link = link else {
                
                print("Error while generating link for \(path)")
                
                if let error = error {
                    
                    HearthManager.shared.sharedDefaults?.set(error.localizedDescription,
                                                             forKey: HearthManager.shared.hearthNotificationErrorKey)
                    
                    print("Error: \(error)")
                }
                else {
                    
                    HearthManager.shared.sharedDefaults?.set("Wait 1-2 seconds for the file to be added and try again.",
                                                             forKey: HearthManager.shared.hearthNotificationErrorKey)
                }

                return
            }
            
            let pasteBoard = NSPasteboard.general
            pasteBoard.clearContents()
            pasteBoard.writeObjects([link as NSString])
                        
            HearthManager.shared.sharedDefaults?.set(path, forKey: HearthManager.shared.hearthNotificationPathKey)
            HearthManager.shared.sharedDefaults?.set(link, forKey: HearthManager.shared.hearthNotificationLinkKey)
        })
    }

    @IBAction func openHearthFolder(_ sender: AnyObject?) {
        
        guard let directoryURL = FIFinderSyncController.default().directoryURLs.first else {
            return
        }

        NSWorkspace.shared.open(directoryURL)
    }
}

