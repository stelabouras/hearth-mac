//
//  AppDelegate.swift
//  helper
//
//  Created by Stelios Petrakis on 11/02/2018.
//  Copyright © 2018 Horizon Video Technologies. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        
        let basePath = Bundle.main.bundlePath as NSString
        var path = basePath.deletingLastPathComponent as NSString
        path = path.deletingLastPathComponent as NSString
        path = path.deletingLastPathComponent as NSString
        path = path.deletingLastPathComponent as NSString
        
        let pathToExecutable = path.appendingPathComponent("Contents/MacOS/Hearth")

        if NSWorkspace.shared.launchApplication(pathToExecutable) {
            
            print("Launched executable successfully")
        }
        else if NSWorkspace.shared.launchApplication(path as String) {
            
            print("Launched app successfully")
        }
        else {
            
            print("Failed to launch")
        }
        
        NSApplication.shared.terminate(self)
    }
}

